if [ ! "$1" ]; then
	for file in `ls -lR ./ | grep -P "^\-" | awk {'print $10'}`; do
		if file -s $file | grep -P "\bimage\b"; then
			echo "$file is an image"
		fi
	done
else
	for file in `ls -lR "$1" | grep -P "^\-" | awk {'print $10'}`; do
		if file -s $file | grep -P "\bimage\b"; then
			echo "$file is an image"
		fi
	done
fi

if [ ! "$1"]; then
	base="./"
else
	base="$1"
fi

for file in `ls -lR "$base" | grep -P "^\-" | awk {'print $10'}`; do
	if file -s $file | grep -P "\bimage data\b"; then
		echo "$file is an image"
	fi
done

find -iregex "^.*\.\(PNG\|JPG\|JPEG\)$"

find . -print > allfiles
file < allfiles | grep *.txt | wc -l

for line in `ls -R1 $base`; do
	if grep ":$" $line; then
		dir=$line
	else
		isimage=test file "$dir/$line" | grep "image data"
	fi
done
